//
//  ViewController.swift
//  NCubeTest
//
//  Created by Anton Vizgin on 2/24/17.
//  Copyright © 2017 NCube. All rights reserved.
//

import UIKit

fileprivate let reuseIdentifier = "ProductCell"

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cartTitle: UILabel!
    
    var stock = Stock.current
    var cart = Cart.current

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.estimatedRowHeight = 40
        tableView.rowHeight = UITableViewAutomaticDimension
        
        cart.productsChanged = { [weak self] in
            self?.cartTitle.text = self?.cart.productsInCart
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func showCart() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "CartViewController") else { return }
        present(vc, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stock.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? ProductCell else { fatalError("No \(reuseIdentifier) Cell") }
        cell.product = stock.products[indexPath.row]
        cell.buttonPressed = { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.stock.products[indexPath.row].amount -= 1
            weakSelf.cart.addItem(weakSelf.stock.products[indexPath.row])
            if weakSelf.stock.products[indexPath.row].amount == 0 { weakSelf.stock.removeItem(indexPath.row) }
            weakSelf.tableView.reloadData()
        }
        
        return cell
    }
}
