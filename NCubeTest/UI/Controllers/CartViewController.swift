//
//  CartViewController.swift
//  NCubeTest
//
//  Created by Anton Vizgin on 2/24/17.
//  Copyright © 2017 NCube. All rights reserved.
//

import UIKit

fileprivate let reuseIdentifier = "ProductCell"

class CartViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var stock = Stock.current
    var cart = Cart.current
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.estimatedRowHeight = 40
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func closeCart() {
        dismiss(animated: true, completion: nil)
    }
}

extension CartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? ProductCell else { fatalError("No \(reuseIdentifier) Cell") }
        cell.product = cart.products[indexPath.row]
        cell.buttonPressed = { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.cart.products[indexPath.row].amount -= 1
            weakSelf.stock.addItem(weakSelf.cart.products[indexPath.row])
            if weakSelf.cart.products[indexPath.row].amount == 0 { weakSelf.cart.removeItem(indexPath.row) }
            weakSelf.tableView.reloadData()
        }
        
        return cell
    }
}
