//
//  ProductCell.swift
//  NCubeTest
//
//  Created by Anton Vizgin on 2/24/17.
//  Copyright © 2017 NCube. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    var buttonPressed: (() -> ())?
    
    var product: Product! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    fileprivate func updateUI() {
        titleLabel.text = product.name
        priceLabel.text = "$\(product.price)"
        amountLabel.text = "\(product.amount)"
    }
    
    @IBAction func buyButtonPressed() {
        buttonPressed?()
    }

}
