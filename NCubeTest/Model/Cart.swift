//
//  Cart.swift
//  NCubeTest
//
//  Created by Anton Vizgin on 2/24/17.
//  Copyright © 2017 NCube. All rights reserved.
//

import UIKit

class Cart: NSObject {
    
    static var current = Cart()
    
    var products = [Product]() {
        didSet {
            productsChanged?()
        }
    }
    fileprivate var totalCost: Int {
        return products.reduce(0, { $0 + ($1.price * $1.amount) })
    }
    fileprivate var totalItems: Int {
        return products.count
    }
    
    var productsInCart: String {
        return totalItems == 0 ? "Cart: 0 items" : "Cart: \(totalItems) items of $\(totalCost)"
    }
    
    var productsChanged: (() -> ())?
    
    func addItem(_ item: Product) {
        if let index = products.index(where: { $0.name == item.name }) {
            products[index].amount += 1
            productsChanged?()
        } else {
            let product = Product(name: item.name, price: item.price, amount: 1)
            products.append(product)
        }
    }
    
    func removeItem(_ index: Int) {
        products.remove(at: index)
    }
}
