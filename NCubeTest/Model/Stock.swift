//
//  Stock.swift
//  NCubeTest
//
//  Created by Anton Vizgin on 2/24/17.
//  Copyright © 2017 NCube. All rights reserved.
//

import UIKit

class Stock {
    
    static var current = Stock()

    var products: [Product] {
        didSet {
            products = products.sorted(by: { $0.0.price < $0.1.price })
        }
    }
    
    init() {
        products = [Product]()
        for i in 1...20 {
            products.append(Product(name: "\(i)", price: i * 10))
        }
    }
    
    func addItem(_ item: Product) {
        if let index = products.index(where: { $0.name == item.name }) {
            products[index].amount += 1
        } else {
            let product = Product(name: item.name, price: item.price, amount: 1)
            products.append(product)
        }
        Cart.current.productsChanged?()
    }
    
    func removeItem(_ index: Int) {
        products.remove(at: index)
    }
}
