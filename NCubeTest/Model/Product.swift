//
//  Product.swift
//  NCubeTest
//
//  Created by Anton Vizgin on 2/24/17.
//  Copyright © 2017 NCube. All rights reserved.
//

import UIKit

class Product {

    var name: String
    var price: Int
    var amount: Int
    
    init(name: String, price: Int, amount: Int = 20) {
        self.name = name
        self.price = price
        self.amount = amount
    }
    
}
